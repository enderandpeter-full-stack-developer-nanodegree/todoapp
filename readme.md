# Todo List App
A very simple application built with Flask to manage to-do lists.
This is an example project built mostly by following instructions from Udacity's Full Stack Developer Nanodegree program.

## Installation
The following Python packages are required. A `requirements.txt` is coming soon:

* psycopg2
* Flask
* Flask-SQLAlchemy
* Flask-Migrate

## Deployment

Create a [Python virtual environment](https://docs.python.org/3.7/tutorial/venv.html) with the command `python -m venv venv`

# Database Migrations

The `migrations` folder has the Alembic code that creates the tables. The command `flask upgrade` will run them. 

### Dev
Set the following environment variables:
* `FLASK_APP=app.py`
* `FLASK_DEBUG=1`

The command `flask run` will start the app.
