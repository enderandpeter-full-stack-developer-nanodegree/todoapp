from flask import Flask, render_template, request, redirect, url_for, jsonify, abort
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import MetaData
from sqlalchemy.ext.hybrid import hybrid_property
import sys

convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=convention)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:postgres@localhost/todoapp'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app, metadata=metadata)

migrate = Migrate(app, db)

class Todo(db.Model):
    __tablename__ = 'todos'
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(), nullable=False)
    completed = db.Column(db.Boolean, nullable=False, server_default="0")
    list_id = db.Column(
        db.Integer,
        db.ForeignKey('todolists.id', onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False
    )

    def __repr__(self):
        return f'<Todo {self.id} {self.description}>'

class TodoList(db.Model):
    __tablename__ = 'todolists'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    todos = db.relationship('Todo', backref='list', cascade="all, delete-orphan")

    @hybrid_property
    def completed(self):
        return not not [todo.id for todo in self.todos if todo.completed]

    def __repr__(self):
        return f'<TodoList {self.id} {self.name}>'

#db.create_all()


@app.route('/')
def index():
    return redirect(url_for('get_list_todos', list_id=1))


@app.route('/list/create', methods=['POST'])
def create_list():
    error = False
    body = {}
    try:
        name = request.get_json()['name']
        todo_list = TodoList(name=name)
        db.session.add(todo_list)
        db.session.commit()
        body['name'] = todo_list.name
        body['id'] = todo_list.id
    except:
        error = True
        db.session.rollback()
        print(sys.exc_info())
    finally:
        db.session.close()
    if error:
        abort(400)
    else:
        return jsonify(body)

@app.route('/list/<list_id>')
def get_list_todos(list_id):
    return render_template(
        'index.html',
        active_list=TodoList.query.get(list_id),
        lists=TodoList.query.all(),
        todos=Todo.query.filter_by(list_id=list_id).order_by(Todo.id).all()
    )


@app.route('/list/<list_id>/set-completed', methods=['POST'])
def complete_list(list_id):
    error = False
    body = {}

    try:
        completed = request.get_json()['completed']
        Todo.query.filter(Todo.list_id == list_id).update({"completed": completed})
        db.session.commit()
        body['success'] = True
    except:
        error = True
        db.session.rollback()
        print(sys.exc_info())
    finally:
        db.session.close()

    if error:
        abort(400)
    else:
        return jsonify(body)


@app.route('/list/<list_id>/delete', methods=['DELETE'])
def delete_list(list_id):
    error = False
    try:
        print(f'Deleting Todo: {list_id}')
        id = request.get_json()['listId']
        if not id:
            raise Exception(f'ID {id} not found');
        if not id == list_id:
            raise Exception(f'ID {id} not specified in request');
        todoList = TodoList.query.get(list_id)
        db.session.delete(todoList)
        db.session.commit()
    except:
        error = True
        print(sys.exc_info())
        db.session.rollback()
    finally:
        db.session.close()

    if error:
        abort(400)

    return jsonify({'success': True})


@app.route('/todos/create/<list_id>', methods=['POST'])
def create_todo(list_id):
    error = False
    body = {}
    try:
        description = request.get_json()['description']
        todo = Todo(description=description)
        todoList = TodoList.query.get(list_id)
        todo.list = todoList
        db.session.add(todo)
        db.session.commit()
        body['description'] = todo.description
    except:
        error = True
        db.session.rollback()
        print(sys.exc_info())
    finally:
        db.session.close()
    if error:
        abort(400)
    else:
        return jsonify(body)


@app.route('/todos/<todo_id>/set-completed', methods=['POST'])
def set_completed_todo(todo_id):
    error = False
    try:
        completed = request.get_json()['completed']
        print('completed', completed)
        todo = Todo.query.get(todo_id)
        todo.completed = completed
        db.session.commit()
    except:
        error = True
        print(sys.exc_info())
        db.session.rollback()
    finally:
        db.session.close()

    if error:
        abort(400)

    return redirect(url_for('index'))


@app.route('/todos/<todo_id>/delete', methods=['DELETE'])
def delete_todo(todo_id):
    error = False
    try:
        print(f'Deleting Todo: {todo_id}')
        id = request.get_json()['todoId']
        if not id:
            raise Exception(f'ID {id} not found');
        if not id == todo_id:
            raise Exception(f'ID {id} not specified in request');
        todo = Todo.query.get(todo_id)
        db.session.delete(todo)
        db.session.commit()
    except:
        error = True
        print(sys.exc_info())
        db.session.rollback()
    finally:
        db.session.close()
    
    if error:
        abort(400)

    return jsonify({'success': True})
