"""empty message

Revision ID: 1b2ce8cfc084
Revises: d9bf990f0201
Create Date: 2020-02-02 17:45:17.114742

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1b2ce8cfc084'
down_revision = 'd9bf990f0201'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('todos', 'list_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.create_foreign_key('fk_todos_list_id_todolists', 'todos', 'todolists', ['list_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('fk_todos_list_id_todolists', 'todos', type_='foreignkey')
    op.alter_column('todos', 'list_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
